(* Prelude *)

type point2d = int * int
type tetragon = point2d * point2d * point2d * point2d

(* Solution *)

let pairwise_distinct (t:tetragon) =
  let (lup, rup, llp, rlp) = t
  in
  (lup <> rup) && (lup <> llp) && (lup <> rlp) &&
  (rup <> llp) && (rup <> rlp) &&
  (llp <> rlp);;

let wellformed (t:tetragon) =
  let (lup, rup, llp, rlp) = t
  in
  let (lupx, lupy) = lup and
    (llpx, llpy) = llp and
    (rupx, rupy) = rup and
    (rlpx, rlpy) = rlp
  in
  pairwise_distinct (lup, rup, llp, rlp) &&
  lupx < rupx && lupx < rlpx &&
  llpx < rupx && llpx < rlpx &&
  lupy > llpy &&
  rupy > rlpy
;;

let wellformed_bad (lup, rup, llp, rlp) =
  let (lupx, lupy) = lup and
  (llpx, llpy) = llp and
  (rupx, rupy) = rup and
  (rlpx, rlpy) = rlp
  in
  lupx < rupx && lupx < rlpx &&
  llpx < rupx && llpx < rlpx
;;

let rotate_point ((x, y):point2d):point2d =
  (y, -x);;

let ordinate ((_, y):point2d) = y ;;

let highest_first p1 p2 =
  if (ordinate p1) > (ordinate p2)
  then (p1, p2)
  else (p2, p1);;

let reorder (p1, p2, p3, p4):tetragon =
  let ls = List.sort compare [p1;p2;p3;p4]
  in
  let (lup, llp) = highest_first (List.nth ls 0) (List.nth ls 1)
  and (rup, rlp) = highest_first (List.nth ls 2) (List.nth ls 3)
  in
  (lup, rup, llp, rlp)
;;

let rotate_tetragon (t:tetragon) =
  let (p1, p2, p3, p4) = t
  in
  reorder ((rotate_point p1), (rotate_point p2),
           (rotate_point p3), (rotate_point p4))
;;
