(* Prelude *)

type int_ff = int -> int

(* Solution *)

let rec compose = function
  | [] -> (function x -> x)
  | [f] -> f
  | f::rest -> let frest = compose rest
      in
      function x -> f (frest x)
;;

let rec fixedpoint f (start:float) (delta:float) =
  let y = start
  and fy = (f start)
  in
  if abs_float (fy -. y) < delta
  then y
  else fixedpoint f fy delta
;;
