let for_all p l =
  List.fold_left (fun x y -> if (p y) then x else false)
    true
    l
;;

let exists p l =
  List.fold_left (fun x y -> if (p y) then true else x)
    false
    l
;;

let amkcmp x y =
  if (x < y) then 1
  else if (x > y) then -1
  else 0
;;

let sorted cmp = function
  | [] -> true
  | head::rest ->
      let checker = fun x y ->
        match x with
        | None -> None
        | Some value -> if (cmp value y) <= 0 then Some y else None
      in
      let result = List.fold_left checker (Some head) rest
      in
      match result with
      | None -> false
      | Some value -> true
;;
