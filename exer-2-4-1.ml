let min_index a =
  let rec try_min a i smallest =
    if i == (Array.length a) then
      smallest
    else
      let
        new_smallest = if a.(i) < a.(smallest) then i else smallest
      in
      try_min a (i+1) new_smallest
  in
  try_min a 0 0
;;

let min a =
  a.(min_index a)
;;

let it_scales = "yes";;
