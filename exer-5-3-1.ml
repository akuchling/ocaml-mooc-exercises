let rec print_int_list = function
  | [] -> ()
  | x::rest -> let _ = print_int x
      and _ = print_newline ()
      in print_int_list rest
;;

let print_every_other k l =
  let rec aux count modulus l = match l with
    | [] -> ()
    | x::rest ->
        let _ = if count = 0 then
            let _ = print_int x
            in print_newline ()
          else ()
        in aux ((count + 1) mod modulus) modulus rest
  in aux 0 k l
;;

let rec print_list print = function
  | [] -> ()
  | x::rest ->
      let _ = print x
      and _ = print_newline ()
      in print_list print rest
;;
