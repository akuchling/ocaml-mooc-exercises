(* Prelude *)

type trie = Trie of int option * char_to_children
and char_to_children = (char * trie) list

let empty =
  Trie (None, [])

let example =
  Trie (None,
	[('i', Trie (Some 11,
                     [('n', Trie (Some 5, [('n', Trie (Some 9, []))]))]));
	 ('t',
	  Trie (None,
		[('e',
		  Trie (None,
			[('n', Trie (Some 12, [])); ('d', Trie (Some 4, []));
			 ('a', Trie (Some 3, []))]));
		 ('o', Trie (Some 7, []))]));
	 ('A', Trie (Some 15, []))])

(* Solution *)

let rec children_from_char m c = match m with
  | [] -> None
  | (ch, t) :: rest when ch = c -> Some t
  | head :: rest -> children_from_char rest c
;;

let rec update_children m c t = match m with
  | [] -> [(c, t)]
  | (ch, t') :: rest when ch = c -> (ch, t) :: rest
  | head :: rest -> head :: update_children rest c t
;;

let rec lookup trie w =
  match trie with
  | Trie (v, assoc) ->
      if w = ""
      then v
      else
        let ch = String.get w 0
        in
        let suffix = String.sub w 1 ((String.length w) - 1)
        in
        match children_from_char assoc ch with
        | None -> None
        | Some subtrie -> lookup subtrie suffix
;;

let rec insert trie w v =
  match trie with
  | Trie (v', assoc) ->
      if w = ""
      then Trie (Some v, assoc)
      else
        let ch = String.get w 0
        in
        let suffix = String.sub w 1 ((String.length w) - 1)
        in
        match children_from_char assoc ch with
        | None ->
            let subtrie' = insert empty suffix v
            in
            Trie (v', update_children assoc ch subtrie')
        | Some subtrie ->
            let subtrie' = insert subtrie suffix v
            in
            Trie (v', update_children assoc ch subtrie')
;;
