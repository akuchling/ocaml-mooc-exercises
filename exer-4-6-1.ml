let filter p l =
  List.fold_right (fun x y ->
      if p x then x::y else y) l []
;;

let partition p l =
  let checker = fun x y -> match y with
    | (lpos, lneg) when (p x) -> (x::lpos, lneg)
    | (lpos, lneg) -> (lpos, x::lneg)
  in
  List.fold_right checker l ([],[])
;;

let rec sort = function
  | [] -> []
  | [x] -> [x]
  | pivot::rest ->
      let low, high = partition (function x -> x < pivot) rest
      in
      (sort low) @ [pivot] @ (sort high)
;;
