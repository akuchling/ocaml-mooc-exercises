let last_character str =
  let index = (String.length str) - 1 in
  String.get str index ;;

let string_of_bool truth =
  if truth then "true" else "false" ;;
