(* Prelude *)

type 'a bt =
  | Empty
  | Node of 'a bt * 'a * 'a bt ;;

(* Solution *)

let rec height = function
  | Empty -> 0
  | Node (left, elem, right) ->
      let left_h = height left
      and right_h = height right
      in
      1 + (if left_h > right_h
           then left_h
           else right_h)
;;

let rec balanced = function
  | Empty -> true
  | Node (Empty, elem, right) when right <> Empty -> false
  | Node (left, elem, Empty) when left <> Empty -> false
  | Node (left, elem, right) ->
      balanced left && balanced right
;;
