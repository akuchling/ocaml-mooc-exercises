(* Prelude *)

type 'a clist =
  | CSingle of 'a
  | CApp of 'a clist * 'a clist
  | CEmpty

let example =
  CApp (CApp (CSingle 1,
              CSingle 2),
        CApp (CSingle 3,
              CApp (CSingle 4, CEmpty)))

(* Solution *)

let rec to_list = function
  | CEmpty -> []
  | CSingle x -> [x]
  | CApp (x, y) -> (to_list x) @ (to_list y)
;;

let rec of_list = function
  | [] -> CEmpty
  | [x] -> CSingle x
  | x :: rest ->
      CApp ((of_list [x]), (of_list rest))
;;

let append l1 l2 = match (l1, l2) with
  | (CEmpty, l) -> l
  | (l, CEmpty) -> l
  | _ -> CApp (l1, l2)
;;

let rec hd = function
  | CEmpty -> None
  | CSingle x -> Some x
  | CApp (x, y) ->
      let x' = hd x
      in
      if (x' = None) then hd y else x'
;;

let rec tl = function
  | CEmpty -> None
  | CSingle x -> Some CEmpty
  | CApp (CEmpty, y) -> tl y
  | CApp (x, y) ->
      let x' = tl x
      in
      match x' with
        (* should never happen *)
      | None -> tl y
      | Some x'' ->
          Some (CApp (x'', y))
;;
