(* Prelude *)

type exp =
  | EInt of int
  | EAdd of exp * exp
  | EMul of exp * exp

let example =
  EAdd (EInt 1, EMul (EInt 2, EInt 3))

(* Solution *)


let my_example =
  EAdd (EMul (EInt 2, EInt 2), EMul (EInt 3, EInt 3));;

let rec eval e = match e with
  | EInt v -> v
  | EAdd (exp1, exp2) -> (eval exp1) + (eval exp2)
  | EMul (exp1, exp2) -> (eval exp1) * (eval exp2)
;;

let factorize e =
  (* turns a * b + a * c into a * (b + c) *)
  match e with
  | EAdd (EMul (a, b), EMul(a', c)) when a = a' -> EMul(a, EAdd (b,c))
  | _ -> e
;;

let expand e =
  (* turns a * (b + c) into a * b + a * c *)
  match e with
  | EMul (a, EAdd (b, c)) -> EAdd (EMul (a, b), EMul (a, c))
  | _ -> e
;;

let simplify e = match e with
  | EInt v -> e
  | EAdd (left, right) when left = EInt 0 -> right (* adding zero *)
  | EAdd (left, right) when right = EInt 0 -> left
  | EMul (left, right) when left = EInt 0 -> EInt 0 (* multiply by zero *)
  | EMul (left, right) when right = EInt 0 -> EInt 0
  | EMul (left, right) when left = EInt 1 -> right (* multiply by one *)
  | EMul (left, right) when right = EInt 1 -> left
  | _ -> e
;;
