(* Prelude *)

type filesystem =
  (string * node) list
and node =
  | File
  | Dir of filesystem
  | Symlink of string list

(* Solution *)

let rec print_path= function
  | [] -> ()
  | [x] -> print_string x
  | x::rest ->
      let _ = print_string x in
      let _ = print_char '/' in
      print_path rest;;

let rec print_indent lvl =
  if (lvl = 0) then ()
  else
    let _ = print_string "| "
    in print_indent (lvl - 1)
;;

let rec print_file lvl name =
  let _ = print_indent lvl in
  print_string name
;;

let rec print_symlink lvl name path =
  let _ = print_indent lvl in
  let _ = print_string name in
  let _ = print_string " -> "
  in
  print_path path
;;

let rec print_invalid_symlink lvl name path =
  let _ = print_indent lvl in
  let _ = print_string name in
  let _ = print_string " -> "
  in
  print_string "INVALID"
;;

let rec print_dir lvl name =
  let _ = print_indent lvl in
  let _ = print_char '/'
  in
  print_string name
;;

let print_filesystem root =
  let rec print_item lvl = function
    | (name, File) ->
        let _ = print_indent lvl in
        let _ = print_string name
        in print_newline ()
    | (name, Dir subfs) ->
        let _ = print_dir lvl name in
        let _ = print_newline () in
        print_filesystem (lvl+1) subfs
    | (name, Symlink path) ->
        let _ = print_symlink lvl name path
        in print_newline ()

  and print_filesystem lvl = function
    | [] -> ()
    | item::rest ->
        let _ = print_item lvl item in
        print_filesystem lvl rest
  in
  print_filesystem 0 root ;;

let rec resolve sym path =
  (* Remove the final component of the symlink's path *)
  let sym_dir = if sym = [] then [] else List.rev (List.tl (List.rev sym)) in
  let full_path = List.concat [sym_dir;path] in
  let rec remove_dots accum path = match path with
    | [] -> accum
    | x::rest when x = ".." ->
        if (accum = [])
        then remove_dots [] rest
        else
          remove_dots (List.tl accum) rest
    | x::rest ->
        remove_dots (x::accum) rest
  in List.rev (remove_dots [] full_path)
;;

let rec file_exists root path =
  let rec find_name root name = match root with
    | [] -> None
    | (candidate_name, fstype)::rest when candidate_name = name ->
        Some fstype
    | x::rest -> find_name rest name
  in
  match path with
  | [] -> false
  | [name] ->
      let inode = find_name root name
      in
      (match inode with
       | None -> false
       | Some inode' -> (match inode' with
           | File -> true
           | Dir fst -> false
           | Symlink path -> false
         )
      )
  | name::rest ->
      let inode = find_name root name
      in
      (match inode with
       | None -> false
       | Some inode' -> (match inode' with
           | File -> false
           | Symlink path -> false
           | Dir fst ->
               file_exists fst rest
         )
      )
;;

let print_filesystem root =
  let rec print_item lvl fullpath = function
    | (name, File) ->
        let _ = print_indent lvl in
        let _ = print_string name
        in print_newline ()
    | (name, Dir subfs) ->
        let _ = print_dir lvl name in
        let _ = print_newline () in
        let newpath = List.concat [fullpath;[name]] in
        print_filesystem (lvl+1) newpath subfs
    | (name, Symlink path) ->
        let newpath = List.concat [fullpath;[name]] in
        let valid_link = try
            file_exists root (resolve newpath path)
          with | Invalid_argument msg -> false
        in
        let printer = if valid_link
          then print_symlink
          else print_invalid_symlink
        in
        let _ = printer lvl name path
        in print_newline ()

  and print_filesystem lvl fullpath = function
    | [] -> ()
    | item::rest ->
        let _ = print_item lvl fullpath item in
        print_filesystem lvl fullpath rest
  in
  print_filesystem 0 [] root ;;
