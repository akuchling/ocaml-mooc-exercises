(* Prelude *)

type color = Black | Gray | White ;;

(* Solution *)

let lighter c1 c2 =
  match (c1, c2) with
  | (Black, Black) -> false
  | (White, White) -> false
  | (Gray, Gray) -> false
  | (Black, _) -> true
  | (_, White) -> true
  | (White, _) -> false
  | (Gray, Black) -> false
;;
