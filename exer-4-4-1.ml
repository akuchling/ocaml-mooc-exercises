let ccr a b c s =
  s /. (2.0 *. cos (a /. 2.0) *.
        2.0 *. cos (b /. 2.0) *.
        2.0 *. cos (c /. 2.0));;

let ccr = fun a ->
  let aterm = 8.0 *. cos (a /. 2.0)
  in
  fun b ->
    let bterm = aterm *. cos (b /. 2.0)
    in
    fun c ->
      let cterm = bterm *. cos (c /. 2.0)
      in
      fun s ->
        s /. cterm
;;
