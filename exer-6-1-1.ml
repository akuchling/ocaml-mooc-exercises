(* Solution *)

open String;; (* uppercase *)
open List;; (* iter -- must be after String *)
open Digest;; (* to_hex *)

let print_hashes (hashes : Digest.t list) : unit =
  let print_hash h = h |> to_hex |> uppercase |> print_endline in
  iter print_hash hashes;;
