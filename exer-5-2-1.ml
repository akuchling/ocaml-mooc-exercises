(* Prelude *)

type 'a bt =
  | Empty
  | Node of 'a bt * 'a * 'a bt ;;

exception Unbalanced of int ;;

(* Solution *)

let rec height = function
  | Empty -> (0, 1)
  | Node (t, _, t') ->
      let lh, lcount = (height t)
      and rh, rcount = (height t')
      in
      (1 + (max lh rh), lcount+rcount) ;;

let rec balanced = function
  | Empty -> (true, 1)
  | Node (t, _, t') ->
      let lbool, lcount = balanced t
      and rbool, rcount = balanced t'
      in
      if not lbool then (false, lcount)
      else if not rbool then (false, lcount + rcount)
      else let lh, lcount' = height t
        and rh, rcount' = height t'
        in ((lh = rh), lcount + rcount + lcount' + rcount') ;;

let rec bal_height bst =
  let aux = function
    | Empty -> (0, 1)
    | Node (t, _, t') ->
        let lh, lcount = bal_height t
        and rh, rcount = bal_height t'
        in
        if (lh <> rh) then
          raise (Unbalanced (lcount + rcount))
        else (lh+1, lcount + rcount)
  in

  aux bst
;;

let balanced_fast bst =
  try
    let height, visits = bal_height bst
    in
    (true, visits)
  with
    Unbalanced visits -> (false, visits);;
