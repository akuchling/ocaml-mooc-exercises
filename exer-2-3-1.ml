(* Prelude *)

type point  = { x : float; y : float; z : float }
type dpoint = { dx : float; dy : float; dz : float }
type physical_object = { position : point; velocity : dpoint }

(* Solution *)

let move p dp =
  {x = p.x +. dp.dx; y = p.y +. dp.dy ; z = p.z +. dp.dz};;

let next obj =
  {position = move obj.position obj.velocity; velocity = obj.velocity};;

let distance p1 p2 =
  sqrt ((p1.x -. p2.x) ** 2.0 +. (p1.y -. p2.y) ** 2.0 +. (p1.z -. p2.z) ** 2.0);;

let will_collide_soon obj1 obj2 =
  let obj1' = next obj1
  and obj2' = next obj2
  in
  (distance obj1'.position obj2'.position) <= 2.0
;;
