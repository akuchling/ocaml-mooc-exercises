(* Prelude *)

type index = Index of int

(* Solution *)

let read a index = match index with
  | Index i -> a.(i)
;;

let inside a index = match index with
  | Index i -> i >= 0 && i < (Array.length a)
;;

let next = function
  | Index i -> Index (i + 1)
;;

let min_index a =
  let rec helper smallest cur = match cur with
    | Index i when i = Array.length a -> smallest
    | Index i -> match smallest with
      | Index sm -> helper (Index (if a.(i) < a.(sm) then i else sm)) (Index (i+1))
  in
  if (Array.length a) = 0 then
    raise (Invalid_argument "index out of bounds")
  else
    helper (Index 0) (Index 0)
;;
