(* Prelude *)

let is_multiple i x = i mod x = 0

(* Solution *)

let output_multiples x n m =
  for i = n to m do
    if is_multiple i x then (print_int i ; print_string ",") else ()
  done
;;

exception Abort_loop;;

let display_sign_until_zero f m =
  let print_result = function
    | i when i<0 -> print_string "negative\n"
    | i when i>0 -> print_string "positive\n"
    | 0 -> print_string "zero\n" ; raise Abort_loop
  in
  try
    for i = 0 to m do
      let result = f i
      in
      print_result result
    done
  with abort_loop -> ()
;;
