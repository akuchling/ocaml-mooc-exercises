let is_sorted a =
  let rec compare_first_two a i =
    if i = ((Array.length a) - 1) then true
    else
    if (String.compare a.(i) a.(i+1)) >= 0
    then false
    else compare_first_two a (1+i)
  in
  if (Array.length a) <= 1
  then true
  else compare_first_two a 0
;;

let find dict word =
  let rec find_index dict word (s, e) =
    if e < s
    then (-1)
    else
      let mid = (s + e) / 2
      in
      let cmp = String.compare dict.(mid) word
      in
      if cmp = 0
      then mid
      else if cmp < 0
      then find_index dict word ((mid + 1), e)
      else find_index dict word (s, (mid - 1))
  in
  find_index dict word (0, ((Array.length dict)-1))
;;
