(* Prelude *)

type 'a xlist =
  { mutable pointer : 'a cell }
and 'a cell =
  | Nil
  | List of 'a * 'a xlist ;;

let nil () =
  { pointer = Nil } ;;

let cons elt rest =
  { pointer = List (elt, rest) } ;;

exception Empty_xlist ;;

(* Solution *)

let head l = match l.pointer with
  | Nil -> raise Empty_xlist
  | List (elem, tail) -> elem
;;

let tail l = match l.pointer with
  | Nil -> raise Empty_xlist
  | List (elem, tail) -> tail
;;

let add a l =
  let copy = {pointer=l.pointer} in
  let newcell = List (a, copy)
  in
  l.pointer <- newcell;;

let chop l = match l.pointer with
  | Nil -> raise Empty_xlist
  | List (elem, tail) ->
      l.pointer <- tail.pointer
;;

let rec append l l' =
  match (l.pointer, l'.pointer) with
  | (Nil, Nil) -> ()
  | (Nil, List(h,t)) ->
      l.pointer <- List (h, t)
  | (List (elem, tail), _) ->
      append tail l'
;;

let rec filter p l =
  match l.pointer with
  | Nil -> ()
  | List (elem, tail) ->
      if (p elem)
      then
        filter p tail
      else
        (l.pointer <- tail.pointer ;
         filter p l )

;;
