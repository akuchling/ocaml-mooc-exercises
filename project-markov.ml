(* -- Part A -------------------------------------------------------------- *)

let is_letter ch =
  let chcode = Char.code ch
  in
  ((Char.code '0') <= chcode && chcode <= (Char.code '9')) ||
  (128 <= chcode && chcode <= 255) ||
  (Char.lowercase ch) <> ch || (Char.uppercase ch) <> ch ;;
let is_not_letter ch = not (is_letter ch);;
let is_sentence_terminator ch = (ch = '.' || ch = '?' || ch = '!');;
let is_punctuation ch = (ch = ';' || ch = ',' || ch = ':' || ch = '-' ||
                         ch = '"' || ch = '\'' || (is_sentence_terminator ch)) ;;


let rec measure_prefix predicate str start =
  let rec aux str index = match str with
    | "" -> 0
    | _ when index = String.length str -> index (* should really use Some/None here *)
    | _ ->
        let ch = (String.get str index)
        in
        if predicate ch then
          aux str (index+1)
        else
          index
  in
  aux str start
;;

let measure_word = measure_prefix is_letter ;;
let measure_separators = measure_prefix (function ch -> not (is_letter ch)) ;;

let words str =
  let i = ref 0
  and length = String.length str
  and result = ref [] (* empty list for result *)
  in
  while !i < length do
    (* skip non-word stuff *)
    i := measure_separators str !i ;
    let start = !i in
    let end' = measure_word str start in
    let word = String.sub str start (end' - start) in
    result :=  word :: !result ;
    i := end'
  done ;
  List.rev !result
;;

let start_token = "START";;
let stop_token = "STOP";;

let pairs words =
  let rec aux words accum =
    match words with
    | [] -> accum
    | [x] -> (x,stop_token) :: accum
    | x::y::rest -> aux (List.tl words) ((x,y) :: accum)
  in
  if words = []
  then []
  else
    let first_word = List.hd words
    in
    (start_token,first_word) :: List.rev (aux words [])
;;

let build_ltable words =
  let rec aux pairs ltable = match pairs with
    | [] -> ltable
    | (first,second) :: rest ->
        let ltable' = (aux rest ltable) in
        let successors = try List.assoc first ltable'
          with Not_found -> []
        in
        let successors' = second :: successors in
        (first, successors') :: ltable'
  in
  aux (pairs words) []
;;

let choose list =
  let rec aux list choice index = match list with
    | [] -> choice
    | x::rest ->
        let choice' = if (Random.int index) = 0 then x else choice
        in
        aux rest choice' (index+1)
  in
  if list = []
  then raise (Invalid_argument "Choice taken from an empty list")
  else
    aux (List.tl list) (List.hd list) 2
;;

let next_in_ltable table word =
  let successors = List.assoc word table
  in
  choose successors
;;

let walk_ltable table =
  let symbol = ref start_token
  and output = ref []
  in
  while (!symbol <> stop_token) do
    symbol := next_in_ltable table !symbol ;
    if !symbol <> stop_token then
      output := !symbol :: !output
    else
      ()
  done ;
  List.rev !output
;;

(* -- Part B -------------------------------------------------------------- *)

let compute_distribution l =
  let rec merge list accum = match list with
    | [] -> accum
    | word::rest ->
        let accum' = merge rest accum in
        let count = try List.assoc word accum'
          with Not_found -> 0
        and accum'' = List.remove_assoc word accum'
        in
        (word, (count+1)) :: accum''
  in
  let amounts = merge l [] in
  let total = List.fold_left (+) 0 (
      List.map (function pair -> let (word, count) = pair in count) amounts)
  in
  {total=total; amounts=amounts}
;;

let build_htable words =
  let rec make_listtable pairs table = match pairs with
    | [] -> table
    | (first,second) :: rest ->
        let wordlist = try Hashtbl.find table first
          with Not_found -> []
        in
        Hashtbl.replace table first (second::wordlist) ;
        make_listtable rest table
  in
  let lt = make_listtable (pairs words) (Hashtbl.create 1000) in
  let ht = Hashtbl.create (Hashtbl.length lt)
  in
  Hashtbl.iter (fun key wordlist ->
      let dist = compute_distribution wordlist
      in Hashtbl.add ht key dist) lt ;
  ht
;;

exception Choice_made of string;;

let next_in_htable table word =
  let dist = Hashtbl.find table word in
  let choice = Random.int (dist.total) in
  let rec choose_h amounts index = match amounts with
    | [] -> raise (Invalid_argument "index too large")
    | (word,count) :: rest when count > index -> word
    | (word,count) :: rest -> choose_h rest (index-count)
  in
  choose_h (dist.amounts) choice
;;

let walk_htable table =
  let symbol = ref start_token
  and output = ref []
  in
  while (!symbol <> stop_token) do
    symbol := next_in_htable table !symbol ;
    if !symbol <> stop_token then
      output := !symbol :: !output
    else
      ()
  done ;
  List.rev !output
;;

(* -- Part C -------------------------------------------------------------- *)

let break_sentences str =
  let i = ref 0
  and length = String.length str
  and result = ref [] (* empty list for result *)
  in
  while !i < length do
    (* look for sentence terminators *)
    let start = !i in
    let end' = measure_prefix (function ch -> not (is_sentence_terminator ch))
        str start in
    let at_end = (end' = length) in
    let sentence = String.sub str start (end' - start +
                                         (if at_end then 0 else 1)) in
    result :=  sentence :: !result ;
    i := end' + (if at_end then 0 else 1)
  done ;
  List.rev !result
;;

let words2 str =
  let i = ref 0
  and length = String.length str
  and result = ref [] (* empty list for result *)
  in
  while !i < length do
    (* skip non-word stuff *)
    i := measure_prefix (function ch -> not ((is_letter ch) ||
                                             (is_punctuation ch)))
        str !i ;
    if !i < length then
      let start = !i in
      let ch = (String.get str start)
      in
      (* include punctuation as a word *)
      if is_punctuation ch then
        let word = String.make 1 ch in
        result :=  word :: !result ; i := start + 1
      else
        let end' = measure_word str start in
        let word = String.sub str start (end' - start) in
        result :=  word :: !result ;
        i := end'
    else
      ()
  done ;
  List.rev !result
;;

let sentences str =
  let sl = break_sentences str
  in
  List.filter (fun lst -> (List.length lst) > 0) (List.map words2 sl) ;;

let rec start = function
  | 0 -> []
  | x when x > 0 -> start_token :: (start (x - 1))
;;

let rec stop = function
  | 0 -> []
  | x when x > 0 -> stop_token :: (stop (x - 1))
;;

let shift l x =
  (List.tl l) @ [x] ;;

let stream words length =
  let rec aux words length token accum = match words with
    | [] -> token::accum
    | word::rest ->
        aux rest length (shift token word) (token::accum)
  in
  List.rev (aux (words @ [stop_token])
              length (start length) [])
;;

let remove_last lst =
  let flipped = List.rev lst
  in
  ((List.hd flipped), List.rev (List.tl flipped))

;;
let build_ptable words preflength =
  let rec make_listtable tuples table = match tuples with
    | [] -> table
    | tuple :: rest ->
        let word, key = remove_last tuple in
        let wordlist = try Hashtbl.find table key
          with Not_found -> []
        in
        Hashtbl.replace table key (word::wordlist) ;
        make_listtable rest table
  in
  let token_stream = List.tl (stream words (preflength + 1)) in
  let lt = make_listtable token_stream (Hashtbl.create 1000) in
  let ht = Hashtbl.create (Hashtbl.length lt)
  in
  Hashtbl.iter (fun key wordlist ->
      let dist = compute_distribution wordlist
      in Hashtbl.add ht key dist) lt ;
  {prefix_length=preflength; table=ht}

;;
let walk_ptable { table ; prefix_length = pl } =
  let context = ref (start pl)
  and symbol = ref ""
  and output = ref []
  in
  while (!symbol <> stop_token) do
    symbol := next_in_htable table !context ;
    if !symbol <> stop_token then begin
      output := (!symbol :: !output) ;
      context := (shift !context !symbol) ;
    end
    else
      ()
  done ;
  List.rev !output
;;

let rec merge_assoc_list dest src = match src with
  | [] -> dest ;
  | (key,srcvalue)::rest ->
      let destvalue = try List.assoc key dest
        with Not_found -> 0
      in
      let dest' = (key,srcvalue+destvalue):: (List.remove_assoc key dest)
      in
      merge_assoc_list dest' rest
;;

let merge_distribution dest src =
  {total = dest.total + src.total;
   amounts = merge_assoc_list dest.amounts src.amounts}
;;

let empty_distribution = {total = 0; amounts=[]};;

let merge_ptables tl =
  let rec merge_table dest src =
    Hashtbl.iter (
      fun k v ->
        let current = try Hashtbl.find dest k
          with Not_found -> empty_distribution
        in
        Hashtbl.replace dest k (merge_distribution current v)
    ) src
  and merge dest lst = match lst with
    | [] -> dest
    | src :: rest ->
        if (dest.prefix_length <> src.prefix_length)
        then
          raise (Invalid_argument "Prefix lengths don't match")
        else begin
          merge_table dest.table src.table ;
          merge dest rest
        end
  in
  if tl = []
  then raise (Invalid_argument "Empty list of ptables")
  else
    merge (List.hd tl) (List.tl tl)
;;
