(* Prelude *)

type report = message list
and message = string * status
and status = Successful | Failed

type 'a result = Ok of 'a | Error of exn

(* Solution *)

let exec f x =
  try
    Ok (f x)
  with
    e -> Error e
;;

let compare user reference to_string = match (user, reference) with
  | (Ok u,Ok r) ->
      if (u = r)
      then
        let msg = String.concat " " ["got correct value";(to_string r)]
        in (msg, Successful)
      else
        let msg = String.concat " " ["got unexpected value";(to_string u)]
        in (msg, Failed)
  | (Error e,Ok r) ->
      let msg = String.concat " " ["got unexpected exception";(exn_to_string e)]
      in (msg, Failed)
  | (Ok u,Error e) ->
      let msg = String.concat " " ["got unexpected value";(to_string u)]
      in (msg, Failed)
  | (Error u, Error r) ->
      if (u = r)
      then
        let msg = String.concat " " ["got correct exception";(exn_to_string r)]
        in (msg, Successful)
      else
        let msg = String.concat " " ["got unexpected exception";(exn_to_string u)]
        in (msg, Failed)

;;

let test user reference sample to_string =
  let rec aux count accum =
    if (count = 0)
    then accum
    else
      let refdata = (sample ())
      in
      let result_u = exec user refdata
      and result_r = exec reference refdata
      in
      let (result_msg, status) = compare result_u result_r to_string
      in
      let t = ((result_msg, status):message)
      in
      aux (count-1) (t::accum)
  in
  List.rev (aux 10 []);;
