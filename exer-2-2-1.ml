let exchange k =
  (k mod 10) * 10 + (k / 10)
;;

let is_valid_answer (grand_father_age, grand_son_age) =
  (grand_father_age = grand_son_age * 4) &&
  ((exchange grand_father_age) * 3 = (exchange grand_son_age))
;;

let failure = (-1, -1);;

let rec find (max_grand_father_age, min_grand_son_age) =
  let rec try_gson_age (grand_father_age, grand_son_age) =
    if is_valid_answer (grand_father_age, grand_son_age)
    then (grand_father_age, grand_son_age)
    else
    if (grand_son_age < grand_father_age) then
      try_gson_age (grand_father_age, grand_son_age + 1)
    else failure
  in
  let trial = try_gson_age (max_grand_father_age, min_grand_son_age)
  in
  if trial <> failure
  then trial
  else
  if max_grand_father_age > min_grand_son_age then
    find (max_grand_father_age - 1, min_grand_son_age)
  else failure
;;
