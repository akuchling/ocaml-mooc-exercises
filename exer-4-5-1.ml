(* Prelude *)

type 'a tree =
    Node of 'a tree * 'a * 'a tree
  | Leaf of 'a;;

(* Solution *)

let wrap l =
  List.map (function elem -> [elem]) l
;;

let rec tree_map f = function
  | Leaf elem -> Leaf (f elem)
  | Node (lchild, elem, rchild) -> Node ((tree_map f lchild),
                                         (f elem),
                                         (tree_map f rchild))
;;
