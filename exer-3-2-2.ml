let rec mem x l =
  match l with
  | [] -> false
  | head::rest when head = x -> true
  | head::rest -> mem x rest
;;

let rec assoc l key = match l with
  | [] -> None
  | (head::rest) ->
      let (k,v) = head
      in
      if k = key
      then Some v
      else assoc rest key
;;

let reverse l1 =
  let rec helper l1 result = match l1 with
    | [] -> result
    | head::rest -> helper rest (head :: result)
  in
  helper l1 []
;;

let append (l1:int list) (l2:int list) =
  let rec helper l1 l2 result =
    match (l1, l2) with
    | ([], []) -> result
    | ([], (head::rest)) -> helper ([]:int list) rest (head :: result)
    | (head::rest, _) -> helper rest l2 (head :: result)
  in
  reverse (helper l1 l2 [])
;;

let combine l1 l2 =
  let rec helper l1 l2 result = match (l1, l2) with
    | ([], []) -> result
    | ((h1::rest1), (h2::rest2)) ->
        helper rest1 rest2 ((h1, h2)::result)
  in
  reverse (helper l1 l2 [])
;;
