(* Prelude *)

type stack = int array
exception Full
exception Empty

(* Solution *)

let create size =
  Array.init (size+1) (fun x -> 0);;

let push buf elt =
  let size = buf.(0)
  and length = Array.length buf
  in
  if size = (length-1) then raise Full
  else begin
    buf.(0) <- size + 1 ;
    buf.(size+1) <- elt
  end;;

let append buf arr =
  for i = (Array.length arr) -1 downto 0 do
    push buf arr.(i)
  done
;;

let pop buf =
  let size = buf.(0)
  in
  if (size = 0) then raise Empty else begin
    let elem = buf.(size) in
    buf.(0) <- size-1 ; elem
  end;;
