
let rec last_element = function
  | [] -> (invalid_arg "last_element")
  | [x] -> x
  | _::rest -> last_element rest
;;

let rec is_sorted = function
  | [] -> true
  | [x] -> true
  | x::y::rest -> if x < y then is_sorted (y::rest) else false
;;
