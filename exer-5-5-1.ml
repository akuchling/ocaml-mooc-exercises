let rotate a =
  let n = Array.length a in
  try
    let v = a.(0) in
    for i = 0 to n-2 do
      a.(i) <- a.(i+1)
    done;
    a.(n-1)<-v
  with Invalid_argument msg -> ();;

let rec rotate_by a n =
  let n' = if n<0 then (Array.length a) + n else n
  in
  if n' > 0 then
    for i = 1 to n' do
      rotate a
    done
  else
    ()
;;
