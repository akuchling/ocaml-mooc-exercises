(* Prelude *)

type operation =
    Op of string * operation * operation
  | Value of int

type env = (string * (int -> int -> int)) list

(* Solution *)

let rec lookup_function name = function
  | [] -> invalid_arg "lookup_function"
  | (key,func)::tl when key = name -> func
  | hd::tl -> lookup_function name tl
;;

let rec add_function name op env = match env with
  | [] -> [(name, op)]
  | (key, func)::tl when key = name -> (name, op) :: tl
  | hd::tl -> hd::(add_function name op tl)
;;

let my_env = add_function "min"
    (fun (a:int) (b:int) -> if (a < b) then a else b) initial_env
;;

let rec compute env op = match op with
  | Value x -> x
  | Op (name, lhs, rhs) ->
      let func = lookup_function name env
      in
      func (compute env lhs) (compute env rhs)
;;

let rec compute_eff env op = match op with
  | Value x -> x
  | Op (name, lhs, rhs) ->
      (lookup_function name env) (compute_eff env lhs) (compute_eff env rhs)
;;
