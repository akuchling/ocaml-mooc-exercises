(* Prelude *)

module type GenericTrie = sig
  type 'a char_table
  type 'a trie = Trie of 'a option * 'a trie char_table
  val empty : unit -> 'a trie
  val insert : 'a trie -> string -> 'a -> 'a trie
  val lookup : 'a trie -> string -> 'a option
end

(* Solution *)

module CharHashedType =
struct
  type t = char
  let equal c1 c2 = (c1 = c2)
  let hash c = int_of_char c
end ;;

module CharHashtbl = Hashtbl.Make (CharHashedType);;

module type GenericTrie = sig
  type 'a char_table
  type 'a trie = Trie of 'a option * 'a trie char_table
  val empty : unit -> 'a trie
  val insert : 'a trie -> string -> 'a -> 'a trie
  val lookup : 'a trie -> string -> 'a option
end

module Trie : GenericTrie
  with type 'a char_table = 'a CharHashtbl.t =
struct
  type 'a char_table = 'a CharHashtbl.t
  type 'a trie = Trie of 'a option * 'a trie char_table

  let empty () = Trie (None, (CharHashtbl.create 5));;

  let rec lookup trie w = match trie with
    | Trie (elem, ctbl) ->
        if w = "" then elem else
          let ch = String.get w 0
          and remainder = String.sub w 1 ((String.length w) - 1)
          in
          try
            let subtrie =
              try CharHashtbl.find ctbl ch
              with Not_found -> empty()
            in
            lookup subtrie remainder
          with Not_found ->
            None
  ;;

  let rec insert trie w v = match trie with
    | Trie (elem, ctbl) ->
        if w = "" then
          Trie (Some v, ctbl)
        else
          let ch = String.get w 0
          and remainder = String.sub w 1 ((String.length w) - 1) in
          let subtrie =
            try
              CharHashtbl.find ctbl ch
            with Not_found ->
              empty()
          in
          let newsubtrie = (insert subtrie remainder v)
          in
          let tblcopy = CharHashtbl.copy ctbl in
          let _ = (CharHashtbl.replace tblcopy ch newsubtrie)
          in
          Trie (elem, tblcopy)
  ;;

end
