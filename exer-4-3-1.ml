let rec equal_on_common l1 l2 = match l1,l2 with
  | [],_ -> true
  | _,[] -> true
  | h1::r1,h2::r2 -> h1=h2 && equal_on_common r1 r2;;

(* desired type: val equal_on_common : 'a list -> 'a list -> bool = <fun> *)

let rec equal_on_common3 = function
  | [] -> (function l2 -> true)
  | hd1::tl1 -> (function
      | [] -> true
      | hd2::tl2 -> hd1 == hd2 && (equal_on_common3 tl1 tl2)
    )
;;
let equal_on_common = equal_on_common3
