(* Prelude *)

type date =
  { year : int; month : int; day : int;
    hour : int; minute : int }

let the_origin_of_time =
  { year = 1; month = 1; day = 1;
hour = 0; minute = 0 }

(* Solution *)

let wellformed date =
  date.year >= 1 &&
  date.month >= 1 && date.month <= 5 &&
  date.day >= 1 && date.day <= 4 &&
  date.hour >= 0 && date.hour <= 2 &&
  date.minute >= 0 && date.minute <= 1;;

let int_of_date date =
  (* returns the date represented as a count of minutes since the epoch *)
  let timecount date = date.minute +
                       date.hour * 2 +
                       date.day * 2 * 3 +
                       date.month * 2 * 3 * 4 +
                       date.year * 2 * 3 * 4 * 5
  in
  (timecount date) - (timecount the_origin_of_time)
;;

let of_int diff =
  {year = the_origin_of_time.year + diff / 2 / 3 / 4 / 5;
   month = the_origin_of_time.month + (diff mod (2*3*4*5)) / 2 / 3 / 4 ;
   day = the_origin_of_time.day + (diff mod (2*3*4)) / 2 / 3;
   hour = (diff mod (2 * 3)) / 2;
   minute = diff mod 2}
;;

let date_of_int diff =
  {year = the_origin_of_time.year + diff / 2 / 3 / 4 / 5;
   month = the_origin_of_time.month + (diff mod (2*3*4*5)) / 2 / 3 / 4 ;
   day = the_origin_of_time.day + (diff mod (2*3*4)) / 2 / 3;
   hour = (diff mod (2 * 3)) / 2;
   minute = diff mod 2}
;;

let next date =
  of_int (1 + int_of_date date)
;;

let of_int = date_of_int ;;
