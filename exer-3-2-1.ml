(* Prelude *)

type queue = int list * int list

(* Solution *)

let is_empty (front, back) =
  front = [] && back = [];;

let enqueue x (front, back) =
  (front, (x :: back));;

let split l =
  let rec divide input index split_index l1 l2 =
    if index == List.length input
    then (l2, (List.rev l1))
    else let elem = (List.nth input index)
      in
      if (index < split_index) then
        divide input (index + 1) split_index (elem :: l1) l2
      else
        divide input (index + 1) split_index l1 (elem :: l2)
  in
  divide l 0 ((List.length l) / 2) [] []
;;

let dequeue_complex (front, back) = match front with
  | [] -> (match List.rev_append back [] with
      | [] -> (0, (front, back))
      | tail :: tl -> (tail, (front, (List.rev_append tl []))) )
  | head :: hl ->
      (head, (hl, back))
;;

let dequeue = dequeue_complex ;;
